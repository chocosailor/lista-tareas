import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Events } from 'ionic-angular';
import {ListadotareasProvider} from '../../providers/listadotareas/listadotareas';

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {
    titulo: string = "";
    tareas: string[];
    tarea: string;

    constructor(public navCtrl: NavController,private navParams: NavParams, public alerCtrl: AlertController, private listadoTareasProvider: ListadotareasProvider,public events: Events,public alertCtrl: AlertController) {
        this.titulo = this.navParams.get('titulo');
        this.tareas = this.listadoTareasProvider.getTareas(this.titulo);
    }

    agregar() {
        if(this.tarea == "" || this.tarea == null){
            const alert = this.alertCtrl.create({
                title: 'Tarea',
                subTitle: 'No puedes crear una tarea en blanco!',
                buttons: ['OK']
              });
              alert.present();
        }else{
            this.tareas = this.listadoTareasProvider.addTarea(this.titulo,this.tarea)
            this.tarea = "";
        }
    }

    borrar(tarea) {
        var index = this.tareas.indexOf(tarea, 0);
        this.tareas = this.listadoTareasProvider.deleteTarea(this.titulo,index);
    }

    borrarLista(){
        let confirm = this.alerCtrl.create({
            title: 'Borrar lista de tareas',
            message: '¿Estas seguro de querer borrar la lista de tareas "' + this.titulo + '"?',
            buttons: [
              {
                text: 'No',
                handler: () => {
                  console.log('Click en no');
                }
              },
              {
                text: 'Sí',
                handler: () => {
                    this.events.publish('lista:borrada', this.titulo);
                }
              }
            ]
          });
          confirm.present()
    }
}
