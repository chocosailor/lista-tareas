import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Events, AlertController } from 'ionic-angular';

@Component({
    selector: 'page-add-list',
    templateUrl: 'add-list.html'
})
export class AddListPage {
    @ViewChild(Nav) nav: Nav;
    NuevaLista: string;

    constructor(public navCtrl: NavController,public events: Events,public alertCtrl: AlertController) {

    }

    agregar() {
        if(this.NuevaLista == "" || this.NuevaLista == null){
            const alert = this.alertCtrl.create({
                title: 'Lista',
                subTitle: 'No puedes crear una lista de tareas sin titulo!',
                buttons: ['OK']
              });
              alert.present();
        }else{
            this.events.publish('lista:agregada', this.NuevaLista);
            this.NuevaLista = "";
        }
    }
}
