import { Component } from '@angular/core';
import { NavController, AlertController  } from 'ionic-angular';
import {ListadotareasProvider} from '../../providers/listadotareas/listadotareas';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    titulo: string = "Tareas principales"
    tareas: string[];
    tarea: string;

    constructor(public navCtrl: NavController, private listadoTareasProvider: ListadotareasProvider,public alertCtrl: AlertController) {
      this.listadoTareasProvider.addListadoTareas({ title: 'Tareas principales', component: HomePage, tareas: [] });
      this.tareas = this.listadoTareasProvider.getTareas(this.titulo);
    }

    agregar() {
        if(this.tarea == "" || this.tarea == null){
            const alert = this.alertCtrl.create({
                title: 'Tarea',
                subTitle: 'No puedes crear una tarea en blanco!',
                buttons: ['OK']
              });
              alert.present();
        }else{
            this.tareas = this.listadoTareasProvider.addTarea(this.titulo,this.tarea)
            this.tarea = "";
        }
    }

    borrar(tarea) {
        var index = this.tareas.indexOf(tarea, 0);
        this.tareas = this.listadoTareasProvider.deleteTarea(this.titulo,index);
    }
}
