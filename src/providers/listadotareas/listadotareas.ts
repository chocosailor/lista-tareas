import { Injectable } from '@angular/core';
//import { Storage } from '@ionic/storage';

/*
  Generated class for the ListadotareasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.

  FALTA AGREGAR EL STORAGE


*/
@Injectable()
export class ListadotareasProvider {
  listadoTareas: Array<{title: string, component: any, tareas:string[]}>;

  //constructor(private storage: Storage) {
  constructor() {
    this.listadoTareas = [];
  }

  getListadoTareas() {
    return this.listadoTareas;
  }

  getListadoEspecifico(title){
    this.listadoTareas.forEach(element => {
      if(element.title == title){
        return element;
      }
    });
  }

  addListadoTareas(list){
    var check = false;
    this.listadoTareas.forEach(element => {
      if(element.title == list.title){
        check = true;
      }
    });
    if(!check){
      this.listadoTareas.push(list);
    }
    return this.listadoTareas;
  }

  deleteListadoTareas(title: string){
    var ind = 0;
    this.listadoTareas.forEach(function(element,index,array){
      if(element.title == title){
        ind = index;
      }
    });
    if (ind != 0) {
      this.listadoTareas.splice(ind, 1);
    }
    return this.listadoTareas;
  }

  getTareas(title: string): string[]{
    let val = [];
    this.listadoTareas.forEach(element => {
      if(element.title == title){
        val = element.tareas;
      }
    });
    return val;
  }

  addTarea(title: string,tarea: string): string[]{
    let val = [];
    this.listadoTareas.forEach(element => {
      if(element.title == title){
        element.tareas.push(tarea);
        val = element.tareas;
      }
    });
    return val;
  }

  deleteTarea(title: string,index: number): string[]{
    let val = [];
    this.listadoTareas.forEach(element => {
      if(element.title == title){
        if (index > -1) {
            element.tareas.splice(index, 1);
        }
        val = element.tareas;
      }
    });
    return val;
  }
}
