import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ListadotareasProvider} from '../providers/listadotareas/listadotareas';

import { HomePage } from '../pages/home/home';
import { AddListPage } from '../pages/add-list/add-list';
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private listadoTareasProvider: ListadotareasProvider
  ) {
    this.splashScreen.show();
    this.pages = this.listadoTareasProvider.getListadoTareas();
    events.subscribe('lista:agregada', (titulo) => {
      this.pages = this.listadoTareasProvider.addListadoTareas({title:titulo,component:ListPage,tareas:[]});
      this.nav.setRoot(ListPage,{titulo:titulo});
    });
    events.subscribe('lista:borrada', (titulo) => {
      this.pages = this.listadoTareasProvider.deleteListadoTareas(titulo);
      this.nav.setRoot(HomePage);
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component,{titulo:page.title});
  }

  agregarLista() {
    this.nav.setRoot(AddListPage);
  }

}

